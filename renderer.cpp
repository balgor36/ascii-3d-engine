#include "renderer.h"

Renderer::Renderer()
{
    paletteSize = colorPalette.length();
	lightDirection = glm::vec3(0.2f, 0.4f, 0.5f);
	assert(ambientLighting < paletteSize);
	logStream.open("log.txt");
}

Renderer::~Renderer()
{
	logStream.close();
}

void Renderer::Render(Mesh &obj, const glm::vec3& camFront, const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection)
{
    //auto coords = toScreenSpace(m_coords);

	glm::mat4 mvp = projection * view * model;
	glm::mat4 normalMatrix = view * model;

    // Do transformation
    auto indices = obj.GetIndices();
    auto vertices = obj.GetVertices();
    auto normals = obj.GetNormals();
    auto colors = obj.GetColors(); 
    std::array<vertex, 3> currCoords;
    for(int i = 0; i < indices.size(); i+= 3){
        if(i+2 >= indices.size()){
            break;
        }
        glm::vec3 color[3];
        /*
        if(colors.size() > 0){
            color[0] = colors[indices[i]];
            color[1] = colors[indices[i]+1];
            color[2] = colors[indices[i]+2];
        }*/
        // multiplying normals with model matrix isn't neccesery unless we apply scaling on the object
        
        currCoords = {vertex(mvp*glm::vec4(vertices[indices[i]],1.0), color[0],  normalMatrix*glm::vec4(normals[indices[i]], 0.0)),
                    vertex(mvp*glm::vec4(vertices[indices[i+1]],1.0), color[1], /*model */ normalMatrix*glm::vec4(normals[indices[i+1]], 0.0)),
                    vertex(mvp*glm::vec4(vertices[indices[i+2]],1.0), color[2], /*model */ normalMatrix*glm::vec4(normals[indices[i+2]], 0.0))};
        DrawPolygon(currCoords, camFront);
    }
    auto objChildren = obj.GetChildren();
    for(int i = 0; i < objChildren.size(); i++){
        Render(*objChildren[i], camFront, model, view, projection);
    }
}

void Renderer::Print(WINDOW *win)
{
    for(int y = 0; y < screenheight; y++){
        for(int x = 0; x < screenwidth; x++){
            if(world[y][x] > 0){
                wattron(win, COLOR_PAIR(2));
                mvwaddch(win, y, x, colorPalette[world[y][x]]);
                wattroff(win, COLOR_PAIR(2));
            }
        }
    }
}

void Renderer::ClearMap()
{
    for (auto &v: world) {
        std::fill(v.begin(), v.end(), 0);
    }
    for (auto &v: zBuffer) {
        std::fill(v.begin(), v.end(), -1000000.0f);
    }
}

void Renderer::SetScreenSize(int screenwidth, int screenheight)
{
    this->screenwidth = screenwidth;
    this->screenheight = screenheight;
    world.assign(screenheight, std::vector<int>(screenwidth, 0));
    zBuffer.assign(screenheight, std::vector<float>(screenwidth, 0));
}

void Renderer::DrawPolygon(std::array<vertex, 3> coords, const glm::vec3& camFront)
{	
	// without this, z coordinate would not affect output image
	
    for(int i = 0; i < coords.size(); i++){
        coords[i].pos.x /= coords[i].pos.z;
        coords[i].pos.y /= coords[i].pos.z;
    }
    
    std::array<vertex, 3> screenCoords;
    if(!toScreenSpace(coords, screenCoords)){
        return;
    }
   
	glm::vec3 normal = glm::normalize(coords[0].normal+coords[1].normal+coords[2].normal);
    // Do rasterzation
    if(std::tie(screenCoords[1].pos.y, screenCoords[1].pos.x) < std::tie(screenCoords[0].pos.y, screenCoords[0].pos.x)) {
        std::swap(screenCoords[0], screenCoords[1]);
    }
    if(std::tie(screenCoords[2].pos.y, screenCoords[2].pos.x) < std::tie(screenCoords[0].pos.y, screenCoords[0].pos.x)) {
        std::swap(screenCoords[0], screenCoords[2]);
    }
    if(std::tie(screenCoords[2].pos.y, screenCoords[2].pos.x) < std::tie(screenCoords[1].pos.y, screenCoords[1].pos.x)) {
        std::swap(screenCoords[1], screenCoords[2]);
    }
    /*
    glm::vec3 edge1 = coords[1].pos - coords[0].pos;
    glm::vec3 edge2 = coords[2].pos - coords[0].pos;
    
    glm::vec3 faceNormal = glm::cross(edge1, edge2);
    glm::vec3 vertexNormal = coords[0].normal;
    float dot = glm::dot(faceNormal, vertexNormal);
    glm::vec3 normal = (dot < 0.0f) ? -faceNormal : faceNormal;
    */
	//glm::vec3 normal = glm::normalize(glm::cross(edge1, edge2);
	
	/*
	bool isBackface = glm::dot(camFront, normal) >= 0;
	if(isBackface){
		return;
	}
	*/
	
    int ytop = screenCoords[0].pos.y;
    int ymid = screenCoords[1].pos.y;
    int ybot = screenCoords[2].pos.y;

    // triangle has no area
    if(ytop == ybot){
        return;
    }

    // false - left side, true - right side
    bool shortside = (screenCoords[1].pos.y - screenCoords[0].pos.y) * (screenCoords[2].pos.x - screenCoords[0].pos.x) < (screenCoords[1].pos.x - screenCoords[0].pos.x) * (screenCoords[2].pos.y - screenCoords[0].pos.y);

    Line3 left = shortside ? Line3(screenCoords[2], screenCoords[0]) : Line3(screenCoords[2], screenCoords[1]);
    Line3 right = shortside ? Line3(screenCoords[2], screenCoords[1]) : Line3(screenCoords[2], screenCoords[0]);
	
    std::map<int, glm::vec4> map_coords2;
	
    for(int y = ybot; y >= ytop; y--){
        if(y == ymid){
            if(shortside){
                right = Line3(screenCoords[1], screenCoords[0]);
            }
            else{
                left = Line3(screenCoords[1], screenCoords[0]);
            }
        }
        //int x1 = left.b.pos.x + (left.a.pos.x - left.b.pos.x) * (y - left.b.pos.y) / (left.a.pos.y - left.b.pos.y);
        int x1 = left.a.pos.x + ( (left.b.pos.x - left.a.pos.x) / (left.b.pos.y - left.a.pos.y) ) * (y - left.a.pos.y);
        float z1 = left.a.pos.z + ( (left.b.pos.z - left.a.pos.z) / (left.b.pos.y - left.a.pos.y) ) * (y - left.a.pos.y);
        //float z1 = left.b.pos.z + (left.a.pos.z - left.b.pos.z) * (y - left.b.pos.y) / (left.a.pos.y - left.b.pos.y);
        
        //a to b
        // left.a.pos.z ---> start
        // left.b.pos.z ---> end
        //float z1Inverse = 1.0/left.a.pos + ( (1.0/left.b.pos.z - 1.0/left.a.pos.z) /)
        
        //int x2 = right.b.pos.x + (right.a.pos.x - right.b.pos.x) * (y - right.b.pos.y) / (right.a.pos.y - right.b.pos.y);        
        int x2 = right.a.pos.x + ( (right.b.pos.x - right.a.pos.x) / (right.b.pos.y - right.a.pos.y) ) * (y - right.a.pos.y);   
        float z2 = right.a.pos.z + ( (right.b.pos.z - right.a.pos.z) / (right.b.pos.y - right.a.pos.y) ) * (y - right.a.pos.y);
        
        //float z2 = right.b.pos.z + (right.a.pos.z - right.b.pos.z) * (y - right.b.pos.y) / (right.a.pos.y - right.b.pos.y);
        
        //logStream << left.a.pos[2] << " " << left.b.pos[2] << " " << z1 << "            " <<  right.a.pos[2] << " " << right.b.pos[2] << " " << z2 << '\n';
        
        // y1 --- z1
        // starting position = left.b.pos[2]
        // distance = (left.a.pos[2] - left.b.pos[2])
        // (y - left.b.pos[1]) / (left.a.pos[1] - left.b.pos[1])
        // y2 --- z2
        map_coords2.insert(std::pair<int, glm::vec4>(y, glm::vec4(x1, x2, z1, z2)));
    }
    // i.first --> y coordinate
    // i.second.x --> left x coord
    // i.second.y --> right x coord
    // i.second.z --> left z coord
    // i.second.w --> right z coord
    
    for(auto& i : map_coords2){
        for(int x = i.second.x, y=i.first; x < i.second.y; x++){
            if(y >= 0 && y < screenheight && x >=0 && x < screenwidth){
                //int colorX = x * paletteSize/(float)screenwidth;
                //int colorY = i.first * paletteSize/(float)screenheight;
                //17/(float)screenheight
                
                float z = i.second.z + (x-i.second.x)/(i.second.y-i.second.x) * (i.second.w - i.second.z);
                //logStream << i.second.z << " " << i.second.w << " " << z << "   " << i.second.z + (x-i.second.x)/(i.second.y-i.second.x) * (i.second.w - i.second.z) << '\n';
                if(z <= zBuffer[i.first][x]){
					//logStream << z << " " << zBuffer[i.first][x] << '\n';
					continue;
				}
                zBuffer[i.first][x] = z;
                
				
                auto norm = glm::normalize(normal);
                //glm::vec3 fragPos(x, i.first, z);
                glm::vec3 lightDir = glm::normalize(lightDirection);
                float diff = std::max(glm::dot(norm, lightDir), 0.0f) * (paletteSize-1);
				diff = std::max((float)ambientLighting, std::min((float)paletteSize-1, diff));
                world[i.first][x] = diff;
                
            } else {
				continue;
			}
        }
    }
}

vertex Renderer::map(vertex in) {
    vertex result;
    result.color = in.color;
    result.normal = in.normal;
    result.pos.x = (((float)screenwidth-1.0f) / (2.0f)) * (in.pos.x + 1.0f);
    result.pos.y = (((float)screenheight) - ((((float)screenheight-1.0f) / (2.0f)) * (in.pos.y + 1.0f))) - 1.0f;
    result.pos.z = -in.pos.z;
    return result;
 }

// calculating coords from -1...1 to screenwidth...screenheight
bool Renderer::toScreenSpace(std::array<vertex, 3>& coords, std::array<vertex, 3>& output){
	int verticesBehindScreen = 0;
    for(int i = 0; i < 3; i++){
        auto result = map(coords[i]);
        if((result.pos.x < 0 || result.pos.x >= screenwidth) || (result.pos.y < 0 || result.pos.y >= screenheight)){
            verticesBehindScreen++;
        }
        output[i] = result;
    }
    if(verticesBehindScreen == 3)
		return false;
    return true;
}

void Renderer::SetLightDirection(glm::vec3& lightDirection){
	this->lightDirection = lightDirection;
}
