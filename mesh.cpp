#include "mesh.h"

Mesh::Mesh()
{
    // triangle
    /*
    vertices = {
        glm::vec3(-1.0f, 1.0f, 0.0f),
        glm::vec3(-1.0f, -1.0f, 0.0f),
        glm::vec3(1.0f, -1.0f, 0.0f),
        glm::vec3(1.0f, 1.0f, 0.0f)
    };
    indices = {
        0,1,2,
        2,0,3
    };
    */

    // cube
    /*
    vertices = {
        glm::vec3(-1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f, -1.0f, 1.0f),
        glm::vec3(1.0f, -1.0f, 1.0f),
        glm::vec3(1.0f, 1.0f, 1.0f),
        glm::vec3(-1.0f, 1.0f, -1.0f),
        glm::vec3(-1.0f, -1.0f, -1.0f),
        glm::vec3(1.0f, -1.0f, -1.0f),
        glm::vec3(1.0f, 1.0f, -1.0f)
    };

    colors = {
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 0.0f)
    };

    indices = {
        0,1,2,
        2,0,3,
        3,4,0,
        3,7,4,
        6,4,5,
        6,7,4,
        2,5,1,
        2,6,5,
        3,2,6,
        6,7,3,
        0,1,5,
        5,4,0
    };
    */
}

Mesh::Mesh(std::vector<glm::vec3> vertices,
    std::vector<int> indices,
    std::vector<glm::vec3> normals):
    vertices(vertices),indices(indices),normals(normals){}

Mesh::Mesh(std::vector<glm::vec3> vertices,
    std::vector<glm::vec3> colors,
    std::vector<glm::vec3> normals,
    std::vector<int> indices):
    vertices(vertices),indices(indices),normals(normals),colors(colors){}

Mesh::~Mesh()
{

}

std::vector<glm::vec3> &Mesh::GetVertices()
{
    return vertices;
}

std::vector<glm::vec3> &Mesh::GetColors()
{
    return colors;
}

std::vector<glm::vec3> &Mesh::GetNormals()
{
    return normals;
}

std::vector<int> &Mesh::GetIndices()
{
    return indices;
}

std::vector<std::shared_ptr<Mesh>> &Mesh::GetChildren(){
    return children;
}

void Mesh::AddChild(Mesh object){
    children.push_back(std::make_shared<Mesh>(object));
}

void Mesh::SetTransform(glm::mat4x4 transform){
    this->transform = transform;
}

glm::mat4x4 Mesh::GetTransform(){
    return transform;
}
