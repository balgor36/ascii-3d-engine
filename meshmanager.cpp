#include "meshmanager.h"

MeshManager::MeshManager(){}
MeshManager::~MeshManager(){}

bool MeshManager::ImportMesh(std::string path, Mesh& obj){
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_GenNormals | aiProcess_Triangulate );

    if(scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode || !scene)
    {
        std::cout << importer.GetErrorString() << std::endl;
        return false;
    }
    RecursiveProcess(scene->mRootNode, scene);
    obj = this->object;
    return true;
}

void MeshManager::RecursiveProcess(aiNode* node, const aiScene* scene)
{
    // process
    for(int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        //numChilds = node->mNumChildren;
        ProcessMesh(mesh, scene);
        //first = false;
    }
    
    for(int i = 0; i < node->mNumChildren; i++)
    {
        RecursiveProcess(node->mChildren[i], scene);
    }
}

void MeshManager::ProcessMesh(aiMesh* mesh, const aiScene* scene)
{
    std::vector<glm::vec3> vertexData;
    std::vector<glm::vec3> normalsData;
    std::vector<int> indicesData;

    for(int i = 0; i < mesh->mNumVertices; i++)
    {
        glm::vec3 tempVec;

        tempVec.x = mesh->mVertices[i].x;
        tempVec.y = mesh->mVertices[i].y;
        tempVec.z = mesh->mVertices[i].z;

        vertexData.push_back(tempVec);
        
        tempVec.x = mesh->mNormals[i].x;
        tempVec.y = mesh->mNormals[i].y;
        tempVec.z = mesh->mNormals[i].z;
        
        normalsData.push_back(tempVec);
    }

    for(int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        indicesData.push_back(face.mIndices[0]);
        indicesData.push_back(face.mIndices[1]);
        indicesData.push_back(face.mIndices[2]);
    }

    if(isRootMesh){
        object = Mesh(vertexData, indicesData, normalsData);
        lastUpdatedMesh = &object;
        isRootMesh = false;
    }
    else {
        lastUpdatedMesh->AddChild((Mesh(vertexData, indicesData, normalsData)));
        //lastUpdatedMesh = childMeshs[childMeshs.size()-1];
    }
    /*
    if(first){
        objects.push_back(Mesh(vertexData, indicesData));
        size_t meshSize = objects.size();
        lastMesh = objects[meshSize-1];
    }
    else{
        std::vector<Mesh*>& childMeshes = lastMesh->GetChildMeshes();

        childMeshes.push_back(new Mesh(&vertexData, &normalData, &uvData, &tangentData, &bitangentData, &colorData, &indicesData, &path, &name, &m_material));
        if(numChilds > 0){
            lastMesh = childMeshes[lastMesh->GetChildMeshes().size() - 1];
        }
    }*/
}

