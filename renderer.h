#ifndef RENDERER_H
#define RENDERER_H

#include "mesh.h"

#include <ncurses.h>
#include <vector>
#include <array>
#include <tuple>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

struct vertex{
    vertex() {};
    vertex(glm::vec3 pos, glm::vec3 color, glm::vec3 normal) : pos(pos), color(color), normal(normal) {};
    glm::vec3 pos;
    glm::vec3 color;
    glm::vec3 normal;
};

struct Line2{
    Line2() {};
    Line2(glm::vec2 a, glm::vec2 b) : a(a), b(b) {};
    glm::vec2 a;
    glm::vec2 b;
};

struct Line3{
    Line3() {};
    Line3(vertex a, vertex b) : a(a), b(b) {};
    // позиція
    vertex a;
    vertex b;
};

class Renderer
{
public:
    Renderer();
    ~Renderer();
    void Render(Mesh& obj, const glm::vec3& camFront, const glm::mat4& model, const glm::mat4& view, const glm::mat4& projection);
    void Print(WINDOW* win);
    void ClearMap();
    void SetScreenSize(int screenwidth, int screenheight);
    void SetLightDirection(glm::vec3& lightDirection);
private:
    void DrawPolygon(std::array<vertex, 3> coords, const glm::vec3& camFront); // Setting values to the map
    vertex map(vertex in);
    bool toScreenSpace(std::array<vertex, 3>& coords, std::array<vertex, 3>& output);

    std::vector<std::vector<int>> world;
    std::vector<std::vector<float>> zBuffer;
    int screenwidth, screenheight;
    std::string colorPalette = (" .'`^\",:;Il!i><~+_-?][}{1)(|\\/tfjrxnuvczXYUJCLQ0OZmwqpdbkhao*#MW&8%B@$");
    int paletteSize;
    
    glm::vec3 lightDirection;
    int ambientLighting = 3;
    std::ofstream logStream;
};
#endif // RENDERER_H
